/*
 * Geef de laatste 2 cijfers uit een getal dat minstens uit 2 cijfers bestaat.
 */

public class ExtraMOef02 {

	public static void main(String[] args) {
		int getal;
		double rest;
		getal = Invoer.leesInt("Geef een getal dat minstens uit 2 getallen bestaat: ");
		rest = (getal % 100);
//				bv. 123 delen door 100 = 1.23. De rest na deling is 23.0 (rest is een double)
		getal = (int) rest;
//				casten naar (int) 23.0 = 23 
		System.out.println("De laatste 2 cijfers van het getal zijn: " + getal);
	} 

}
