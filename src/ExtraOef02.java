/*
 * Schrijf een programma dat prijs exclusief BTW berekent en afgerond op 2 decimalen afdrukt.
 * Via het toetsenbord wordt de prijs inclusief BTW en het BTW percentage ingegeven.
 */

public class ExtraOef02 {

	public static void main(String[] args) {
		double prijsIncl, prijsExcl;
		int BTWPercent;
		prijsIncl = Invoer.leesDouble("Geef de prijs inclusief BTW in: ");
		BTWPercent = Invoer.leesInt("Geef het BTW percentage in: ");
		prijsExcl = prijsIncl / (1 + BTWPercent / 100.0); 
//					bv. 242 / (1 + 21 / 100) = 242 / (1 + 0.21) = 242 / 1.21 = 200
		prijsExcl = (int) (prijsExcl * 100 + 0.5)  / (double) 100;
		System.out.println("Het totale bedrag exclusief BTW is :" + prijsExcl);
	}

}
