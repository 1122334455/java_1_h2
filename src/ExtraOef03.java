/*
 * Schrijf een programma dat het BTW percentage berekent en afgekapt op 2 decimalen afdrukt.
 * Via het toetsenbord wordt de prijs inclusief BTW en de prijs exclusief BTW ingegeven.
 * Opmerking: een percentage van 21% moet afgedrukt worden als 0.21.
 */

public class ExtraOef03 {

	public static void main(String[] args) {
		double btwIncl, btwExcl, btwPercentage;
		btwIncl = Invoer.leesDouble("Geef het bedrag inclusief BTW in: ");
		btwExcl = Invoer.leesDouble("Geef het bedrag exclusief BTW in: ");
		btwPercentage = (btwIncl / btwExcl) % 1;
//		bijvoorbeld: 777/555=1.4 -> rest na deling met 1 = 0.4 
		System.out.println("Het BTW percentage bedraagt:" + btwPercentage);
	}

}
