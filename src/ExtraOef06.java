/*
 * Wat kost mijn auto? Prijsbewuste personen willen weten hoeveel hun auto echt kost. Achtereen volgens wordt ingevoerd:
 * aantal afgelegde kilometers, verbruik in l per 100 km, prijs van 1l brandstof.
 * Als uitvoer wordt verlangd:
 * de totale kosten per jaar voor het opgegeven aantal kilomter, de kostprijs per km rijden.
 * Beide resultaten wordt afgekapt op 2 decimalen na de komma.
 */

public class ExtraOef06 {

	public static void main(String[] args) {
		/* TODO Auto-generated method stub
		
		int kiloMeters;
		double verbruik, prijsLiter, kostJaar, kostKilometer;
		kiloMeters = Invoer.leesInt("Geef het aantal gereden kilometers per jaar in: ");
		verbruik = Invoer.leesDouble("Geef het verbruik in l/km in: ");
		prijsLiter = Invoer.leesDouble("Geef de prijs van 1 liter brandstof in: ");
		kostJaar = (kiloMeters /100) * (verbruik * prijsLiter);
		kostKilometer = (verbruik * prijsLiter) / 100;
		System.out.println("De kost voor 1 jaar bedraagt " + kostJaar + " euro");
		System.out.println("De kost per kilometer bedraagt  " + kostKilometer + " euro");*/
		
		int afgKm;
		double verbruik, prijsL, prijsKm, prijsJaar;
		afgKm = Invoer.leesInt("Geef het aantal gereden kilometers per jaar in: ");
		verbruik = Invoer.leesDouble("Geef het verbruik in l/km in: ");
		prijsL = Invoer.leesDouble("Geef de prijs van 1 liter brand");
		prijsKm = (verbruik * prijsL) / 100;
		prijsJaar = prijsKm * afgKm;
		prijsJaar = (int) (prijsJaar * 100) / 100;
		prijsKm = (int) (prijsKm * 100) / 100;
		System.out.println("De kost voor 1 jaar bedraagt " + prijsJaar + " euro");
		System.out.println("De kost per kilometer bedraagt  " + prijsKm + " euro");
	}

}
