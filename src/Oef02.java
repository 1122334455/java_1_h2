/*
 * Voer je lengte en gewicht in via het toetsenbord. Bereken je BMI en druk deze af.
 * Je BMI wordt als volgt berekend: gewicht / lengte ** 2
 */

public class Oef02 {

	public static void main(String[] args) {
		int lengte,gewicht,BMI;
		System.out.println("Geef de lengte in: ");
		lengte = Invoer.leesInt();
		System.out.println("Geef het gewicht in: ");
		gewicht = Invoer.leesInt();
		BMI = gewicht / lengte * lengte;
//		BMI = gewicht / Math.pow(lengte, 2);
		System.out.println("Je BMI is: " + BMI);
	}

}
