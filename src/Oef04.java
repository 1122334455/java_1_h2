/*
 * U geeft via het toetsenbord het aantal graden Fahrenheit in, de computer berekent het aantal graden Celcius, afgerond op 1 decimaal na de komma.
 * Dit aantal wordt afgedrukt.
 */

public class Oef04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double gradenFahr,gradenCel;
		System.out.println("Geef het aantal graden in Fahrenheit in: ");
		gradenFahr = Invoer.leesDouble();
		gradenCel = 5.0/9 * (gradenFahr - 32);
//		gradenCel = (double)5/9 * (gradenFahr - 32);		
		gradenCel = gradenCel * 10;
		gradenCel = (int) gradenCel;
		gradenCel = gradenCel / 10;
//		gradenCel = (int)(gradenCel * 10 + 0.5);
//		gradenCel = gradenCel / 10;
//		Op 2 lijnen ipv 3
		System.out.println("Het aantal graden in Celcius is: " + gradenCel);	
	}

}
