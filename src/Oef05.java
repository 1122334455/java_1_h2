/*
 * Wim volgt het eerste jaar toegepaste informatica, hij heeft slechts een zeer beperkt programma, nl 2 opleidingsonderdelen. 
 * Java (7 studiepunten, waarvan 40% op PE, 60% op het examen) en Netwerkbesturingssytemen (3 studiepunten, waarvan  70% op een opdracht, 30% op het examen).
 * Voer de 4 resultaten in via het toetsenbord, telkens op 20 punten.
 * Druk het totaalresultaat af per opleidingsonderdeel en het totaal behaald percentage (hierbij hou je rekening met het aantal studiepunten). 
 */

public class Oef05 {

	public static void main(String[] args) {
		double deelExJava, eindExJava, opdrNetwerkOS, exNetwerkOS,totaalPercentage,totaalJava, totaalNetwerkOS;
		System.out.println("Geef het aantal punten voor het deelexamen van Java in (op 20): ");
		deelExJava = Invoer.leesInt();
		System.out.println("Geef het aantal punten voor het eindexamen van Java in (op 20): ");
		eindExJava = Invoer.leesInt();
		System.out.println("Geef het aantal punten voor de opdracht van Netwerk OS in (op 20): ");
		opdrNetwerkOS = Invoer.leesInt();
		System.out.println("Geef het aantal punten voor eind examen van Netwerk OS in (op 20): ");
		exNetwerkOS = Invoer.leesInt();
		totaalJava = (int) ((deelExJava / 100) * 40 + (eindExJava / 100) * 60);
		totaalNetwerkOS = (int) ((opdrNetwerkOS / 100 * 70) + (exNetwerkOS / 100 * 30));
		totaalPercentage = (totaalJava/20*7 + totaalNetwerkOS/20*3)*10;
/*		totaalJava = deelExJava * 0.4 + eindExJava * 0.6; Bewerkingen zijn simpeler -> *0.4 ipv / 100 * 40
		totaalNetwerkOS = opdrNetwerkOS * 0.7 + exNetwerkOS * 0.3;
		totaalPercentage = (totaalJava*7 + totaalNetwerkOS*3)/10 * 5; *5 omdat de punten op 20 zijn */
		System.out.println("Het totaal voor Java is: " + totaalJava);
		System.out.println("Het totaal voor Netwerk OS is: " + totaalNetwerkOS);
		System.out.println("Het totale percentage is: " + totaalPercentage);
	}

}
