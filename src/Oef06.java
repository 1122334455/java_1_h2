/*
 * Schrijf een programma dat een ingegeven tijd in seconden omzeten naar uren, minuten en seconden.
 */

public class Oef06 {

	public static void main(String[] args) {
		double tijdSecIngave;
		int tijdUren, tijdMinuten, tijdSeconden;
		System.out.println("Geef de tijd in seconden in: ");
		tijdSecIngave = Invoer.leesInt();
/*		tijdUren = (int) (tijdSecIngave / 3600);
		tijdMinuten = (int) (tijdSecIngave / 60);
		tijdSeconden = (int) (((tijdSecIngave/60) - tijdMinuten) * 60);*/
		tijdUren = (int) (tijdSecIngave / 3600);
		double rest = tijdSecIngave % 3600; // Modulus gebruiken aangezien deze de rest na deling geeft -> ipv (int) en delen
		tijdMinuten = (int) (rest / 60);
		tijdSeconden = (int) (rest % 60);
		System.out.println("Tijd in uren is: " + tijdUren);
		System.out.println("Tijd in minuten is: " + tijdMinuten);
		System.out.println("Tijd in seconden is: " + tijdSeconden);
	}

}