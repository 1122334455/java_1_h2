/* 
 * Zet een bedrag in BEF om naar Euro, dit bedrag geef je in voet het toetsenbord.
*/
public class Voorbeeld01 {

	public static void main(String[] args) {
		int bedragBef;
		double bedragEuro;
		final double KOERS = 40.3399;
		
		System.out.println("Geef het bedrag in BEF :"); //syso + control_space
		bedragBef = Invoer.leesInt();
		
		bedragEuro = bedragBef / KOERS;
		System.out.println("Het bedrag in Euro = " + bedragEuro);
	}

}
